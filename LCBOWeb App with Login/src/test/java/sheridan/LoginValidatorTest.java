package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "AamirAmin" ) );
	}
	
	
	@Test
	public void testIsValidLoginLengthException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Aamir$" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "AamirA" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Aamir" ) );
	}
	
	
	//Test for character
	@Test
	public void testIsValidLoginCharacterRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "AamirAmin1111" ) );
	}
	
	@Test
	public void testIsValidLoginCharacterException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "AmirAmin12$" ) );
	}
	
	@Test
	public void testIsValidLoginCharacterBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Aamir1" ) );
	}
	
	@Test
	public void testIsValidLoginCharacterBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Aami1" ) );
	}
}

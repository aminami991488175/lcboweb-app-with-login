package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
			String numbers =  "^[A-Za-z0-9]*$";

        if(loginName.length() >= 6 && loginName.matches(numbers) ) {
            return true;
        } 
        else {
            return false;
        }
	}
}
